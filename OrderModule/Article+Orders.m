//
//  Article+Orders.m
//  ERP-App
//
//  Created by Sebastian Hagedorn on 14/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "Article+Orders.h"

@implementation Article (Orders)

@dynamic orders;

@end
