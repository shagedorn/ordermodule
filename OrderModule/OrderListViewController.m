//
//  OrderListViewController.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 9/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderListViewController.h"
#import "OrderListDetailViewController.h"
#import "Order.h"

#import <InfrastructureModule/CoreDataAppDelegate.h>
#import <InfrastructureModule/BundleResourceHelperClass.h>

@interface OrderListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *resultsController;
@property (nonatomic, strong) UIPopoverController *currentPopover;

///==========================================
/** Initialisation */
///==========================================

/**
 *  Creates a fetched results controller and initialises it.
 */
- (void)initFetchRequest;

@end

@implementation OrderListViewController {}

#pragma mark - Lifecycle

- (id)init {
    NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"OrderModuleBundle"];
    self = [super initWithNibName:@"OrderListViewController" bundle:myBundle];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFetchRequest];
}

- (void)initFetchRequest {
    // Do any additional setup after loading the view from its nib.

    NSSortDescriptor *sorting = [[NSSortDescriptor alloc] initWithKey:@"orderDescription" ascending:YES];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[[Order class] description]];
    request.sortDescriptors = @[sorting];
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    self.resultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                 managedObjectContext:[appDelegate managedObjectContext]
                                                                   sectionNameKeyPath:nil
                                                                            cacheName:nil];
    self.resultsController.delegate = self;
    NSError *error;
    [self.resultsController performFetch:&error];
    if (error) {
        DLog(@"Error fetiching orders: %@", error);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tab Bar Appearance

- (NSString *)title {
    return NSLocalizedString(@"Order.OrderListViewController.Title", nil);
}

- (UITabBarItem *)tabBarItem {
    NSString *imagePath = [BundleResourceHelperClass pathForResource:@"tab_list"
                                                              ofType:@"png"
                                                            inModule:@"OrderModuleBundle"];
    return [[UITabBarItem alloc] initWithTitle:[self title]
                                         image:[UIImage imageWithContentsOfFile:imagePath]
                                           tag:0];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.resultsController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.resultsController.sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"orderListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    Order *cellOrder = [self.resultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = cellOrder.orderDescription;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@: %d", NSLocalizedString(@"Order.ID", nil),
                                 cellOrder.orderId.integerValue];
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // open popover
    if (self.currentPopover) {
        [self.currentPopover dismissPopoverAnimated:YES];
    }
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    targetRect.origin.x += (self.tableView.frame.size.width/2);

    Order *selectedOrder = [self.resultsController objectAtIndexPath:indexPath];
    OrderListDetailViewController *detailCtr = [[OrderListDetailViewController alloc] initWithOrder:selectedOrder];
    self.currentPopover = [[UIPopoverController alloc] initWithContentViewController:detailCtr];
    [self.currentPopover setPopoverContentSize:CGSizeMake(300, 400)];
    [self.currentPopover presentPopoverFromRect:targetRect
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionLeft
                                       animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 140;
}

#pragma mark - Fetched Controller Delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                                withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                                withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath]
                                        withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
