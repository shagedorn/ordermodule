//
//  OrderByCustomerViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 9/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListViewController.h"

/**
 *  Displays a list of all customers with at least one order.
 */
@interface OrderByCustomerViewController : OrderListViewController

@end
