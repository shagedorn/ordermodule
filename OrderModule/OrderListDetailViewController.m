//
//  OrderListDetailViewController.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderListDetailViewController.h"

#import <CustomerModule/Customer.h>

#import <InfrastructureModule/BundleResourceHelperClass.h>

#import <QRCodeGenerator.h>

@interface OrderListDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *textField;
@property (weak, nonatomic) IBOutlet UIImageView *qrImageView;

@property (strong, nonatomic) Order *myOrder;

@end

@implementation OrderListDetailViewController

- (id)initWithOrder:(Order *)order {
    NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"OrderModuleBundle"];
    if (self = [super initWithNibName:@"OrderListDetailViewController" bundle:myBundle]) {
        self.myOrder = order;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.titleLabel.text = [NSString stringWithFormat:@"%@ (%@)", self.myOrder.orderDescription, self.myOrder.orderId];
    NSString *noOfArticles = NSLocalizedString(@"Order.OrderListDetailViewController.NumberOfArticles", nil);
    NSMutableString *mainText = [NSMutableString stringWithFormat:@"%@: %d", noOfArticles, self.myOrder.articles.count];
    NSString *customerString = NSLocalizedString(@"Customer.CustomerDetailViewController.customer", nil);
    [mainText appendFormat:@"\n\n%@: %@", customerString, self.myOrder.customer.name];

    NSString *qrString = [NSString stringWithFormat:@"%@, %@, %@", self.myOrder.orderDescription, self.myOrder.orderId.stringValue, self.myOrder.customer.name];
    UIImage *qrCode = [QRCodeGenerator qrImageForString:qrString imageSize:130];
    self.qrImageView.image = qrCode;
    self.qrImageView.layer.cornerRadius = 5.0f;
    self.qrImageView.layer.masksToBounds = YES;

    self.textField.text = mainText;
}

@end
