//
//  OrderListViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 9/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  This controller displays a list of all loaded orders in a table view.
 *
 */
@interface OrderListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@end
