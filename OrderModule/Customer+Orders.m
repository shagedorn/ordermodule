//
//  Customer+Orders.m
//  ERP-App
//
//  Created by Sebastian Hagedorn on 14/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "Customer+Orders.h"

@implementation Customer (Orders)

@dynamic orders;

@end