//
//  Article+Orders.h
//  ERP-App
//
//  Created by Sebastian Hagedorn on 14/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <ArticleModule/Article.h>

@class Order;

/**
 *  The category adds a public interface to the Article class
 *  so its orders are accessible.
 *
 *  Orders shall only be accessible when the orders module
 *  is linked.
 */
@interface Article (Orders)

/**
 *  All orders this article is part of. Never set
 *  explicitly (only via its inverse property).
 */
@property (nonatomic, strong) NSSet *orders;

@end

@interface Article (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(Order *)value;
- (void)removeOrdersObject:(Order *)value;
- (void)addOrders:(NSSet *)values;
- (void)removeOrders:(NSSet *)values;

@end