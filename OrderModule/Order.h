//
//  Order.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import <InfrastructureModule/ServerEntity.h>

@class Article, Customer;

/**
 *  An Order comprises possibly many articles
 *  and exactly one customer.
 */
@interface Order : NSManagedObject <ServerEntity>

/**
 *  A unique ID.
 */
@property (nonatomic, retain) NSNumber * orderId;

/**
 *  A short description/name.
 */
@property (nonatomic, retain) NSString * orderDescription;

/**
 *  All articles that are part of this order.
 */
@property (nonatomic, retain) NSSet *articles;

/**
 *  The customer who placed the order.
 */
@property (nonatomic, retain) Customer *customer;

@end

@interface Order (CoreDataGeneratedAccessors)

- (void)addArticlesObject:(Article *)value;
- (void)removeArticlesObject:(Article *)value;
- (void)addArticles:(NSSet *)values;
- (void)removeArticles:(NSSet *)values;

@end
