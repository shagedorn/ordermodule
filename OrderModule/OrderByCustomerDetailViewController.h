//
//  OrderByCustomerDetailViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderListDetailViewController.h"

#import <CustomerModule/Customer.h>

/**
 *  This controller displays the customer's name
 *  along with a list of all the customer's orders.
 *
 *  Designed to be displayed in a popover.
 */
@interface OrderByCustomerDetailViewController : OrderListDetailViewController

///==========================================
/** Initialisation */
///==========================================

/**
 *  Always use this initialiser.
 *
 *  @param customer The customer whose data will be displayed.
 *  @return The initialised controller.
 */
- (id) initWithCustomer:(Customer*)customer;

@end
