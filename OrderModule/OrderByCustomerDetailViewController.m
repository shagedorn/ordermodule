//
//  OrderByCustomerDetailViewController.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "OrderByCustomerDetailViewController.h"
#import "Customer+Orders.h"

#import <InfrastructureModule/BundleResourceHelperClass.h>

@interface OrderByCustomerDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *textField;
@property (strong, nonatomic) Customer *myCustomer;

@end

@implementation OrderByCustomerDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"OrderModuleBundle"];
    self = [super initWithNibName:@"OrderListDetailViewController" bundle:myBundle];
    return self;
}

/**
 *  @param order See OrderListDetailViewController.
 *  @return This class always returns nil.
 *  @warning *Attention:* Do not use this inherited method.
 *  Use initWithCustomer: instead.
 */
- (id)initWithOrder:(Order *)order {
    DLog(@"WARNING: Please use initWithCustomer.");
    return nil;
}

- (id)initWithCustomer:(Customer *)customer {
    if (self = [super init]) {
        self.myCustomer = customer;
    }
    return self;
}

- (void)viewDidLoad {
    NSString *headline = NSLocalizedString(@"Order.OrderByCustomerDetailViewController.Headline", nil);
    self.titleLabel.text = [NSString stringWithFormat:@"%@:\n%@", self.myCustomer.name, headline];

    NSMutableString *orderDescriptions = [NSMutableString string];
    for (Order *order in self.myCustomer.orders) {
        [orderDescriptions appendFormat:@"\n%@", order.orderDescription];
    }
    self.textField.text = orderDescriptions;
}

@end
