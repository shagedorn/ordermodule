//
//  OrderListDetailViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 12/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"

/**
 *  Displays basic information about an order.
 *
 *  Designed to be used within a popover controller.
 */
@interface OrderListDetailViewController : UIViewController

///==========================================
/** Initialisation */
///==========================================

/**
 *  Always use this initialiser.
 *
 *  @param order The order to be displayed.
 *  @return The initialised controller.
 */
- (id) initWithOrder:(Order*)order;

@end
