//
//  OrderViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <InfrastructureModule/Module.h>

/**
 *  This controller displays an OrderListViewController
 *  and an OrderByCustomerViewController in a tab bar
 *  controller.
 *
 *  It also implements the Module protocol.
 */
@interface OrderViewController : UITabBarController <Module>

@end
